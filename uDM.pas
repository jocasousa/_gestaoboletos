unit uDM;

interface

uses
  System.SysUtils, System.Classes, MidasLib, FireDAC.UI.Intf,
  FireDAC.VCLUI.Wait, FireDAC.Phys.FBDef, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, Data.DB,
  FireDAC.Comp.Client, FireDAC.Phys.IBBase, FireDAC.Phys.FB, FireDAC.Comp.UI,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  Datasnap.DBClient, Datasnap.Provider, FireDAC.Comp.DataSet, IniFiles, Vcl.Dialogs, Vcl.Forms;

type
  TDM = class(TDataModule)
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    FDPhysFBDriverLink1: TFDPhysFBDriverLink;
    FDConn: TFDConnection;
    FDBuscaBoleto: TFDQuery;
    DSPBuscaBoleto: TDataSetProvider;
    CDSBuscaBoleto: TClientDataSet;
    FDParametros: TFDQuery;
    DSPParametros: TDataSetProvider;
    CDSParametros: TClientDataSet;
    CDSBuscaBoletoID_BOLETO_EMAIL: TIntegerField;
    CDSBuscaBoletoID_CLIENTE: TIntegerField;
    CDSBuscaBoletoDOCUMENTO: TStringField;
    CDSBuscaBoletoDT_DOCUMENTO: TDateField;
    CDSBuscaBoletoDT_VENCIMENTO: TDateField;
    CDSBuscaBoletoDT_PROCESSAMENTO: TDateField;
    CDSBuscaBoletoNOSSO_NUMERO: TStringField;
    CDSBuscaBoletoVL_DOCUMENTO: TFloatField;
    CDSBuscaBoletoVL_ABATIMENTO: TFloatField;
    CDSBuscaBoletoVL_MORA_JUROS: TFloatField;
    FDConsCliente: TFDQuery;
    DSPConsCliente: TDataSetProvider;
    CDSConCliente: TClientDataSet;
    FDConsDuplicatas: TFDQuery;
    DSPConsDuplicatas: TDataSetProvider;
    CDSConsDuplicatas: TClientDataSet;
    CDSConsDuplicatasID_BOLETO: TIntegerField;
    CDSConsDuplicatasOLD: TStringField;
    CDSConsDuplicatasSITUACAO: TStringField;
    CDSConsDuplicatasID_CLIENTE: TFloatField;
    CDSConsDuplicatasAREA: TStringField;
    CDSConsDuplicatasCD_VENDED: TStringField;
    CDSConsDuplicatasVALOR: TFloatField;
    CDSConsDuplicatasDT_VENC: TDateField;
    CDSConsDuplicatasDT_PAGAM: TDateField;
    CDSConsDuplicatasCONTROLE: TStringField;
    CDSConsDuplicatasVL_PAGO: TFloatField;
    CDSConsDuplicatasFI_CLINICA: TStringField;
    CDSConsDuplicatasTP_PAGAM: TStringField;
    CDSConsDuplicatasCHEQUE: TStringField;
    CDSConsDuplicatasFORMA: TStringField;
    CDSConsDuplicatasDESCRICAOFORMA: TStringField;
    CDSConsDuplicatasUSUARIO_BAIXA: TStringField;
    CDSConsDuplicatasDESCRICAOSERVICO: TStringField;
    CDSConsDuplicatasTP_BOLETO: TStringField;
    CDSConsDuplicatasCHAVE: TStringField;
    CDSConsDuplicatasTP_GERACAO: TStringField;
    CDSConsDuplicatasDT_GERACAO: TDateField;
    FDControle: TFDQuery;
    DSPControle: TDataSetProvider;
    CDSControle: TClientDataSet;
    CDSControleCAMPO: TStringField;
    CDSControleVALOR: TFloatField;
    FDConsBoletos: TFDQuery;
    DSPConsBoletos: TDataSetProvider;
    CDSConsBoletos: TClientDataSet;
    CDSBuscaBoletoID_DUPLICATA: TIntegerField;
    CDSConsBoletosID_CLIENTE: TFloatField;
    CDSConsBoletosRAZAO_SOCIAL: TStringField;
    CDSConsBoletosNOME_FANTASIA: TStringField;
    CDSConsBoletosCNPJ: TStringField;
    CDSConsBoletosSTATUS: TStringField;
    CDSConsBoletosID_BOLETO_EMAIL: TIntegerField;
    CDSConsBoletosID_CLIENTE_1: TIntegerField;
    CDSConsBoletosID_DUPLICATA: TIntegerField;
    CDSConsBoletosDOCUMENTO: TStringField;
    CDSConsBoletosDT_DOCUMENTO: TDateField;
    CDSConsBoletosDT_VENCIMENTO: TDateField;
    CDSConsBoletosDT_PROCESSAMENTO: TDateField;
    CDSConsBoletosNOSSO_NUMERO: TStringField;
    CDSConsBoletosVL_DOCUMENTO: TFloatField;
    CDSConsBoletosVL_ABATIMENTO: TFloatField;
    CDSConsBoletosVL_MORA_JUROS: TFloatField;
    FDSenha: TFDQuery;
    DSPSenha: TDataSetProvider;
    CDSSenha: TClientDataSet;
    CDSSenhaCD_USUARIO: TFloatField;
    CDSSenhaCD_PERMISSAO: TFloatField;
    CDSSenhaNOME: TStringField;
    CDSSenhaLOGIN: TStringField;
    CDSSenhaSENHA: TStringField;
    CDSSenhaID_TECNICO: TFloatField;
    CDSSenhaPODEVENDER: TStringField;
    CDSSenhaCAIXA: TStringField;
    CDSSenhaVERCHAMADO: TStringField;
    CDSSenhaSTATUS: TStringField;
    CDSConsBoletosENDERECO: TStringField;
    CDSConsBoletosBAIRRO: TStringField;
    CDSConsBoletosCIDADE: TStringField;
    CDSConsBoletosCEP: TStringField;
    CDSConsBoletosUF: TStringField;
    CDSConsBoletosNUMERO: TStringField;
    CDSConsBoletosX: TIntegerField;
    CDSBuscaBoletoCARTEIRA: TStringField;
    CDSBuscaBoletoENVIADO: TIntegerField;
    CDSConsBoletosCARTEIRA: TStringField;
    CDSConsBoletosENVIADO: TIntegerField;
    CDSConsBoletosEMAIL: TStringField;
    CDSParametrosSERVIDORSMTPFINANCEIRO: TStringField;
    CDSParametrosUSUARIOSMTPFINANCEIRO: TStringField;
    CDSParametrosSENHASMTPFINANCEIRO: TStringField;
    CDSParametrosPORTASMTPFINANCEIRO: TStringField;
    CDSBuscaBoletoENVIADOEM: TStringField;
    CDSConsBoletosENVIADOEM: TStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure FDConnBeforeConnect(Sender: TObject);
  private
    { Private declarations }
  public
  function Crypt(Action, Src: String): String;
   var CaminhoBD : TIniFile;
         Senha, NomeServidor, Caminho : String;
  end;

var
  DM: TDM;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

function TDM.Crypt(Action, Src: String): String;
var KeyLen : Integer;
  KeyPos : Integer;
  OffSet : Integer;
  Dest, Key : String;
  SrcPos : Integer;
  SrcAsc : Integer;
  TmpSrcAsc : Integer;
  Range : Integer;
begin
  if (Src = '') Then
  begin
    Result:= '';
    Exit;
  end;
  Key :=
'YUQL23KL23DF90WI5E1JAS467NMCXXL6JAOAUWWMCL0AOMM4A4VZYW9KHJUI2347EJHJKDF3424SKL K3LAKDJSL9RTIKJ';
  Dest := '';
  KeyLen := Length(Key);
  KeyPos := 0;
  SrcPos := 0;
  SrcAsc := 0;
  Range := 256;
  if (Action = UpperCase('C')) then
  begin
    Randomize;
    OffSet := Random(Range);
    Dest := Format('%1.2x',[OffSet]);
    for SrcPos := 1 to Length(Src) do
    begin
      Application.ProcessMessages;
      SrcAsc := (Ord(Src[SrcPos]) + OffSet) Mod 255;
      if KeyPos < KeyLen then KeyPos := KeyPos + 1 else KeyPos := 1;
      SrcAsc := SrcAsc Xor Ord(Key[KeyPos]);
      Dest := Dest + Format('%1.2x',[SrcAsc]);
      OffSet := SrcAsc;
    end;
  end
  Else if (Action = UpperCase('D')) then
  begin
    OffSet := StrToIntDef('$'+ copy(Src,1,2),0);
    SrcPos := 3;
  repeat
    SrcAsc := StrToIntDef('$'+ copy(Src,SrcPos,2),0);
    if (KeyPos < KeyLen) Then KeyPos := KeyPos + 1 else KeyPos := 1;
    TmpSrcAsc := SrcAsc Xor Ord(Key[KeyPos]);
    if TmpSrcAsc <= OffSet then TmpSrcAsc := 255 + TmpSrcAsc - OffSet
    else TmpSrcAsc := TmpSrcAsc - OffSet;
    Dest := Dest + Chr(TmpSrcAsc);
    OffSet := SrcAsc;
    SrcPos := SrcPos + 2;
  until (SrcPos >= Length(Src));
  end;
  Result:= Dest;
end;

procedure TDM.DataModuleCreate(Sender: TObject);
begin
  CaminhoBD := TIniFile.Create(ExtractFilePath(Application.ExeName) + 'CaminhoBD.Ini');
  NomeServidor    := Crypt('D', CaminhoBD.ReadString('NOME','NOME','NomedoServidorDB'));
  Caminho := Crypt('D', CaminhoBD.ReadString('CAMINHO', 'CAMINHO', ''));
  Senha := Crypt('D', CaminhoBD.ReadString('FARMAX', 'FARMAX', ''));
  CaminhoBD.Free;
end;

procedure TDM.FDConnBeforeConnect(Sender: TObject);
begin
 try
    FDConn.Params.Values['Database'] := NomeServidor + ':' + Caminho;
    FDConn.Params.Values['User_Name']:= 'sysdba';
    FDConn.Params.Values['Password'] := 'masterkey';
   except on E: Exception do
     ShowMessage(E.Message);
 end;
end;

end.
