program GestaoBoletos;

uses
  Vcl.Forms,
  uPrincipal in 'uPrincipal.pas' {frmPrincipal},
  uDM in 'uDM.pas' {DM: TDataModule},
  uFuncoes in 'uFuncoes.pas',
  uMensagem in 'uMensagem.pas' {FrmMensagem},
  uSenha in 'uSenha.pas' {FrmSenha};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TDM, DM);
  Application.CreateForm(TfrmPrincipal, frmPrincipal);
  Application.CreateForm(TFrmMensagem, FrmMensagem);
  Application.CreateForm(TFrmSenha, FrmSenha);
  Application.Run;
end.
