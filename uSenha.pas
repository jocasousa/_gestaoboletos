unit uSenha;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TFrmSenha = class(TForm)
    EdtSenha: TEdit;
    Label1: TLabel;
    procedure EdtSenhaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmSenha: TFrmSenha;

implementation

{$R *.dfm}

uses uPrincipal;

procedure TFrmSenha.EdtSenhaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

  case Key of
  VK_RETURN:
    begin
      frmPrincipal.SenhaGerencial := EdtSenha.Text;
      Close;
    end;

  VK_ESCAPE:
    begin
      frmPrincipal.SenhaGerencial := '';
      Close;
    end;
  end;

end;

end.
