unit uPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.ComCtrls, Vcl.Grids,
  Vcl.DBGrids, Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.WinXCtrls, ACBrBoleto, ACBrBoletoConversao,
  ACBrBase, ACBrBoletoFCFR, Vcl.Imaging.pngimage, System.Math, Vcl.Buttons,
  frxClass, ACBrMail;

type
  TfrmPrincipal = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    DBGrid1: TDBGrid;
    SBPesquisar: TSearchBox;
    DT1: TDateTimePicker;
    DT2: TDateTimePicker;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    OpenDialog1: TOpenDialog;
    ACBrBoleto: TACBrBoleto;
    ACBrBoletoFCFR1: TACBrBoletoFCFR;
    DSConsBoletos: TDataSource;
    Image1: TImage;
    Label4: TLabel;
    Image2: TImage;
    Image3: TImage;
    ACBrMail1: TACBrMail;
    ProgressBar1: TProgressBar;
    lbProgress: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure SBPesquisarInvokeSearch(Sender: TObject);
    procedure DT2CloseUp(Sender: TObject);
    procedure DT1CloseUp(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Label4Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Image2Click(Sender: TObject);
    procedure Image3Click(Sender: TObject);
  private
    { Private declarations }
  public
   SenhaGerencial : String;
   Procedure ImportarArquivo;
   Procedure Pesquisar;
   Procedure CriarBoletoPDF;
   Procedure EnviarBoletoEmail;
   function FormataNome(sNome: String): string;
  end;

var
  frmPrincipal: TfrmPrincipal;

implementation

{$R *.dfm}

uses uDM, uFuncoes, uSenha;

{ TfrmPrincipal }


procedure TfrmPrincipal.BitBtn1Click(Sender: TObject);
begin
 CriarBoletoPDF;
end;

procedure TfrmPrincipal.CriarBoletoPDF;
var Titulo : TACBrTitulo;
    Mensagem : TStringList;
begin
  ACBrBoletoFCFR1.DirLogo            := ExtractFilePath(Application.ExeName);
  ACBrBoletoFCFR1.FastReportFile     := ExtractFilePath(Application.ExeName) + 'Boleto.fr3';

  ACBrBoleto.Cedente.Agencia         := '1234';
  ACBrBoleto.Cedente.Conta           := '12345';
  ACBrBoleto.Cedente.ContaDigito     := '9';
  ACBrBoleto.Cedente.FantasiaCedente := 'EMPRESA X';
  ACBrBoleto.Cedente.Nome            := 'EMPRESA';
  ACBrBoleto.Cedente.Logradouro      := 'RUA A';
  ACBrBoleto.Cedente.Bairro          := 'SERRA GRANDE';
  ACBrBoleto.Cedente.Cidade          := 'NITEROI';
  ACBrBoleto.Cedente.CEP             := '24342-735';
  ACBrBoleto.Cedente.Telefone        := '(21) 1111-21212';

  Titulo := ACBrBoleto.CriarTituloNaLista;

  with Titulo do
  begin
    Vencimento              := DM.CDSConsBoletosDT_VENCIMENTO.Value;
    DataDocumento           := DM.CDSConsBoletosDT_DOCUMENTO.Value;
    NumeroDocumento         := DM.CDSConsBoletosDOCUMENTO.Value;
    EspecieDoc              := 'DSI';
    Aceite                  := atNao;
    DataProcessamento       := DM.CDSConsBoletosDT_PROCESSAMENTO.Value;
    Carteira                := DM.CDSConsBoletosCARTEIRA.Value;
    NossoNumero             := DM.CDSConsBoletosNOSSO_NUMERO.Value;
    ValorDocumento          := DM.CDSConsBoletosVL_DOCUMENTO.Value;
    Sacado.NomeSacado       := DM.CDSConsBoletosRAZAO_SOCIAL.Value;
    Sacado.CNPJCPF          := DM.CDSConsBoletosCNPJ.Value;
    Sacado.Logradouro       := DM.CDSConsBoletosENDERECO.Value;
    Sacado.Numero           := DM.CDSConsBoletosNUMERO.Value;
    Sacado.Bairro           := DM.CDSConsBoletosBAIRRO.Value;
    Sacado.Cidade           := DM.CDSConsBoletosCIDADE.Value;
    Sacado.UF               := DM.CDSConsBoletosUF.Value;
    Sacado.CEP              := DM.CDSConsBoletosCEP.Value;
    ValorAbatimento         := DM.CDSConsBoletosVL_ABATIMENTO.Value;
    LocalPagamento          := 'EM QUALQUER BANCO OU CORRESP. N�O BANCARIO, MESMO APOS O VENCIMENTO';
    ValorMoraJuros          := DM.CDSConsBoletosVL_MORA_JUROS.Value;
    ValorDesconto           := 0;
    ValorAbatimento         := DM.CDSConsBoletosVL_ABATIMENTO.Value;
    CodigoMoraJuros         := cjIsento;
    OcorrenciaOriginal.Tipo := toRemessaBaixar;
    QtdePagamentoParcial    := 0;
    TipoPagamento           := tpNao_Aceita_Valor_Divergente;
    PercentualMinPagamento  := 0;
    PercentualMaxPagamento  := 0;
    ValorMinPagamento       := 0;
    ValorMaxPagamento       := 0;

    Mensagem := TStringList.Create;
    Mensagem.Add('AP�S O VENCIMENTO COBRABAR MORA DE R$.....1,20 AO DIA | MORA DE 0.40%');
    Mensagem.Add('EFETUAR O PAGAMENTO SOMENTE ATRAV�S DESTE BLOQUETO NA REDE BANCARIA');
    Mensagem.Add('**** N�O CONCEDER DESCONTOS ****');

    ACBrBoleto.AdicionarMensagensPadroes(Titulo, Mensagem);    
    Verso := False;
    ACBrBoleto.Imprimir;
  end;

  ACBrBoleto.ListadeBoletos.Clear;
  Mensagem.Free;
end;

procedure TfrmPrincipal.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
   var
  Check: Integer;
  R: TRect;
  const
clPaleEnviado = TColor($0092D43C);
begin
inherited;


  if ((Sender as TDBGrid).DataSource.DataSet.IsEmpty) then
    Exit;

  // Desenha um checkbox no dbgrid
  if Column.FieldName = 'X' then
  begin
    TDBGrid(Sender).Canvas.FillRect(Rect);

    if ((Sender as TDBGrid).DataSource.Dataset.FieldByName('X').AsInteger = 1) then
      Check := DFCS_CHECKED
    else
      Check := 0;

    R := Rect;
    InflateRect(R, -2, -2); { Diminue o tamanho do CheckBox }
    DrawFrameControl(TDBGrid(Sender).Canvas.Handle, R, DFC_BUTTON,
      DFCS_BUTTONCHECK or Check);

 end;

 //Enviados
 if Column.Field.Dataset.FieldbyName('ENVIADO').Value = 1 then
 begin
     if Column.FieldName <> 'X' then
     begin
      If (gdFocused in State)
       then
     else DBGrid1.Canvas.brush.color := clPaleEnviado;
              DBGrid1.DefaultDrawColumnCell(rect,DataCol,Column,State);
     end;

 end;

end;

procedure TfrmPrincipal.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if (key= VK_RETURN) or (key=VK_SPACE) then
 begin
    if ((Sender as TDBGrid).DataSource.Dataset.IsEmpty) then
        Exit;

    (Sender as TDBGrid).DataSource.Dataset.Edit;

    (Sender as TDBGrid).DataSource.Dataset.FieldByName('X').AsInteger :=
     IfThen((Sender as TDBGrid).DataSource.Dataset.FieldByName('X').AsInteger = 1, 0, 1);
    (Sender as TDBGrid).DataSource.Dataset.Post;

    DBGrid1.Columns[DBGrid1.Columns.grid.SelectedIndex + 1].Field.FocusControl;

    DM.CDSConsBoletos.Next;

    DBGrid1.Columns[0].Field.FocusControl;
  end;
end;

procedure TfrmPrincipal.DT1CloseUp(Sender: TObject);
begin
 Pesquisar;
end;

procedure TfrmPrincipal.DT2CloseUp(Sender: TObject);
begin
 Pesquisar;
end;

procedure TfrmPrincipal.EnviarBoletoEmail;
var Titulo : TACBrTitulo;
    Mensagem, CorpoEmail : TStringList;
    I: Integer;
begin
  ACBrBoletoFCFR1.DirLogo            := ExtractFilePath(Application.ExeName);
  ACBrBoletoFCFR1.FastReportFile     := ExtractFilePath(Application.ExeName) + 'Boleto.fr3';
  ACBrBoletoFCFR1.NomeArquivo        := ExtractFilePath(Application.ExeName) + 'Boletos\Boleto_' + DM.CDSConsBoletosCNPJ.AsString + '_' + StringReplace(DateToStr(DM.CDSConsBoletosDT_VENCIMENTO.Value), '/' , '', [rfReplaceAll]) + '_' + StringReplace(TimeToStr(Now), ':', '', [rfReplaceAll]);

  ACBrBoleto.Cedente.Agencia         := '1234';
  ACBrBoleto.Cedente.Conta           := '12345';
  ACBrBoleto.Cedente.ContaDigito     := '9';
  ACBrBoleto.Cedente.FantasiaCedente := 'EMPRESA X';
  ACBrBoleto.Cedente.Nome            := 'EMPRESA';
  ACBrBoleto.Cedente.Logradouro      := 'RUA A';
  ACBrBoleto.Cedente.Bairro          := 'SERRA GRANDE';
  ACBrBoleto.Cedente.Cidade          := 'NITEROI';
  ACBrBoleto.Cedente.CEP             := '24342-735';
  ACBrBoleto.Cedente.Telefone        := '(21) 1111-21212';

 DM.CDSConsBoletos.First;
 ProgressBar1.Visible  := True;
 lbProgress.Visible    := True;
 ProgressBar1.Min      := 0;
 ProgressBar1.Max      := DM.CDSConsBoletos.RecordCount;
 ProgressBar1.Position := 0;
 Application.ProcessMessages;
 DM.CDSConsBoletos.DisableControls;

 while not DM.CDSConsBoletos.Eof do
 begin
  ProgressBar1.Position := ProgressBar1.Position + 1;

  if DM.CDSConsBoletosX.Value = 1 then
  begin
    ACBrBoleto.ListadeBoletos.Clear;
    Titulo := ACBrBoleto.CriarTituloNaLista;

    with Titulo do
    begin
      Vencimento              := DM.CDSConsBoletosDT_VENCIMENTO.Value;
      DataDocumento           := DM.CDSConsBoletosDT_DOCUMENTO.Value;
      NumeroDocumento         := DM.CDSConsBoletosDOCUMENTO.Value;
      EspecieDoc              := 'DSI';
      Aceite                  := atNao;
      DataProcessamento       := DM.CDSConsBoletosDT_PROCESSAMENTO.Value;
      Carteira                := DM.CDSConsBoletosCARTEIRA.Value;
      NossoNumero             := DM.CDSConsBoletosNOSSO_NUMERO.Value;
      ValorDocumento          := DM.CDSConsBoletosVL_DOCUMENTO.Value;
      Sacado.NomeSacado       := DM.CDSConsBoletosRAZAO_SOCIAL.Value;
      Sacado.CNPJCPF          := DM.CDSConsBoletosCNPJ.Value;
      Sacado.Logradouro       := DM.CDSConsBoletosENDERECO.Value;
      Sacado.Numero           := DM.CDSConsBoletosNUMERO.Value;
      Sacado.Bairro           := DM.CDSConsBoletosBAIRRO.Value;
      Sacado.Cidade           := DM.CDSConsBoletosCIDADE.Value;
      Sacado.UF               := DM.CDSConsBoletosUF.Value;
      Sacado.CEP              := DM.CDSConsBoletosCEP.Value;
      Sacado.Email            := DM.CDSConsBoletosEMAIL.Value;
      ValorAbatimento         := DM.CDSConsBoletosVL_ABATIMENTO.Value;
      LocalPagamento          := 'EM QUALQUER BANCO OU CORRESP. N�O BANCARIO, MESMO APOS O VENCIMENTO';
      ValorMoraJuros          := DM.CDSConsBoletosVL_MORA_JUROS.Value;
      ValorDesconto           := 0;
      ValorAbatimento         := DM.CDSConsBoletosVL_ABATIMENTO.Value;
      CodigoMoraJuros         := cjIsento;
      OcorrenciaOriginal.Tipo := toRemessaBaixar;
      QtdePagamentoParcial    := 0;
      TipoPagamento           := tpNao_Aceita_Valor_Divergente;
      PercentualMinPagamento  := 0;
      PercentualMaxPagamento  := 0;
      ValorMinPagamento       := 0;
      ValorMaxPagamento       := 0;
      Instrucao1              := '';
      Instrucao2              := '';
      Instrucao3              := '';

      Mensagem := TStringList.Create;
      Mensagem.Add('AP�S O VENCIMENTO COBRABAR MORA DE R$.....1,20 AO DIA | MORA DE 0.40%');
      Mensagem.Add('EFETUAR O PAGAMENTO SOMENTE ATRAV�S DESTE BLOQUETO NA REDE BANCARIA');
      Mensagem.Add('**** N�O CONCEDER DESCONTOS ****');

      ACBrBoleto.AdicionarMensagensPadroes(Titulo, Mensagem);
      Verso := False;
    end;

    DM.CDSParametros.Close;
    DM.CDSParametros.Open;

    // Configura��o do servidor SMTP
    ACBrMail1.Clear;
    ACBrMail1.IsHTML   := True;
    ACBrMail1.Subject  := 'Boleto dispon�vel para pagamento';
    ACBrMail1.From     :=  DM.CDSParametrosUSUARIOSMTPFINANCEIRO.Value;
    ACBrMail1.FromName :=  DM.CDSConsBoletosRAZAO_SOCIAL.Value;
    ACBrMail1.Host     := DM.CDSParametrosSERVIDORSMTPFINANCEIRO.Value;
    ACBrMail1.Username := DM.CDSParametrosUSUARIOSMTPFINANCEIRO.Value;
    ACBrMail1.Password := FormataNome(DM.CDSParametrosSENHASMTPFINANCEIRO.Value);
    ACBrMail1.Port     := DM.CDSParametrosPORTASMTPFINANCEIRO.Value;
    ACBrMail1.SetTLS   := False;
    ACBrMail1.SetSSL   := True;


    //Configurando Corpo do E-mail HTML.
    CorpoEmail := TStringList.Create;

    CorpoEmail.Add('<table class="bg-light body" style="outline: 0; width: 100%; min-width: 100%; height: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; font-family: Helvetica, Arial, sans-serif; line-height: 24px; font-weight: normal; font-size: 16px;');
    CorpoEmail.Add('-moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0px; border-collapse: collapse; margin: 0; padding: 0; border: 0;" bgcolor="#FFFFFF">');
    CorpoEmail.Add('<tbody><tr>');
    CorpoEmail.Add('<td style="border-spacing: 0px; border-collapse: collapse; line-height: 24px; font-size: 16px; margin: 0;" align="left" valign="top" bgcolor="#FFFFFF">');
    CorpoEmail.Add('<table class="container" style="font-family: Helvetica, Arial, sans-serif; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0px; border-collapse: collapse; width: 100%;" border="0" cellspacing="0" cellpadding="0">');
    CorpoEmail.Add('<tbody><tr>');
    CorpoEmail.Add('<td style="border-spacing: 0px; border-collapse: collapse; line-height: 24px; font-size: 16px; margin: 0; padding: 0 16px;" align="center">');
    CorpoEmail.Add('<table style="font-family: Helvetica, Arial, sans-serif; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0px; border-collapse: collapse; width: 100%;');
    CorpoEmail.Add('max-width: 600px; margin: 0 auto;" border="0" cellspacing="0" cellpadding="0" align="center">');
    CorpoEmail.Add('<tbody><tr>');
    CorpoEmail.Add('<td style="border-spacing: 0px; border-collapse: collapse; line-height: 24px; font-size: 16px; margin: 0;" align="left">');
    CorpoEmail.Add('<table class="mx-auto" style="font-family: Helvetica, Arial, sans-serif; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0px; border-collapse: collapse; margin: 0 auto;" border="0" cellspacing="0" cellpadding="0" align="center">');
    CorpoEmail.Add('<tbody><tr>');
    CorpoEmail.Add('<td style="border-spacing: 0px; border-collapse: collapse; line-height: 24px; font-size: 16px; margin: 0;" align="left">');
    CorpoEmail.Add('<table class="s-4 w-100" style="width: 100%;" border="0" cellspacing="0" cellpadding="0">');
    CorpoEmail.Add('<tbody><tr>');
    CorpoEmail.Add('<td style="border-spacing: 0px; border-collapse: collapse; line-height: 24px; font-size: 24px; width: 100%; height: 24px; margin: 0;" align="left" height="24"></td>');
    CorpoEmail.Add('</tr></tbody></table>');
    CorpoEmail.Add('<img class="  " style="height: auto; line-height: 100%; outline: none; text-decoration: none; border: 0 none;" src="http://farmax.far.br/wp-content/uploads/2013/05/logo1.jpg" width="170" height="50" />');
    CorpoEmail.Add('<table class="s-3 w-100" style="width: 100%;" border="0" cellspacing="0" cellpadding="0">');
    CorpoEmail.Add('<tbody><tr>');
    CorpoEmail.Add('<td style="border-spacing: 0px; border-collapse: collapse; line-height: 16px; font-size: 16px; width: 100%; height: 16px; margin: 0;" align="left" height="16"></td>');
    CorpoEmail.Add('</tr></tbody></table></td></tr></tbody></table><table class="card " style="font-family: Helvetica, Arial, sans-serif; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0px; border-collapse: separate !important; border-radius: 4px; width: 100%;');
    CorpoEmail.Add('overflow: hidden; border: 1px solid #dee2e6;" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">');
    CorpoEmail.Add('<tbody> <tr>');
    CorpoEmail.Add('<td style="border-spacing: 0px; border-collapse: collapse; line-height: 24px; font-size: 16px; width: 100%; margin: 0;" align="left">');
    CorpoEmail.Add('<div style="border-top-width: 5px; border-top-color: #0059B2; border-top-style: solid;">');
    CorpoEmail.Add('<table class="card-body" style="font-family: Helvetica, Arial, sans-serif; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0px; border-collapse: collapse; width: 100%;" border="0" cellspacing="0" cellpadding="0">');
    CorpoEmail.Add('<tbody><tr>');
    CorpoEmail.Add('<td style="border-spacing: 0px; border-collapse: collapse; line-height: 24px; font-size: 16px; width: 100%; margin: 0px; padding: 20px; text-align: center;" align="left">');
    CorpoEmail.Add('<h4 class="text-center" style="margin-top: 0; margin-bottom: 0; font-weight: 500; color: inherit; vertical-align: baseline; font-size: 24px; line-height: 28.8px;" align="center">Ol�<br /><strong>' + Titulo.Sacado.NomeSacado + '</strong></h4>');
    CorpoEmail.Add('<h4 align="center">Sua fatura j� est� dispon�vel com data de vencimento para ' + DatetoStr(Titulo.Vencimento) + '.</h4>');
    CorpoEmail.Add('<div class="hr " style="width: 100%; margin: 20px 0; border: 0;">');
    CorpoEmail.Add('<table style="font-family: Helvetica, Arial, sans-serif; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0px; border-collapse: collapse; width: 100%;" border="0" cellspacing="0" cellpadding="0">');
    CorpoEmail.Add('<tbody><tr>');
    CorpoEmail.Add('<td style="border-spacing: 0px; border-collapse: collapse; line-height: 24px; font-size: 16px; border-top: 1px solid #dddddd; height: 1px; width: 100%; margin: 0px; text-align: center;" align="left">Segue em anexo o boleto para pagamento!</td>');
    CorpoEmail.Add('</tr></tbody></table></div>');
    CorpoEmail.Add('<p style="text-align: center; font-size: 14px;">Caso tenha ficado com alguma d&uacute;vida, entre em contato com o telefone (21) 2609-3000 ou mande um WhatsApp para (21) 98744-6365</p>');
    CorpoEmail.Add('</td></tr></tbody></table></div></td></tr></tbody></table>');
    CorpoEmail.Add('<table class="s-4 w-100" style="width: 100%;" border="0" cellspacing="0" cellpadding="0">');
    CorpoEmail.Add('<tbody><tr>');
    CorpoEmail.Add('<td style="border-spacing: 0px; border-collapse: collapse; line-height: 24px; font-size: 24px; width: 100%; height: 24px; margin: 0;" align="left" height="24"></td>');
    CorpoEmail.Add('</tr></tbody></table>');
    CorpoEmail.Add('<div class="text-center text-muted" style="color: #636c72;" align="center"><strong>Farmasoft Tecnologia</strong></div>');
    CorpoEmail.Add('<div class="text-center text-muted" style="color: #636c72;" align="center">(21) 2609-3000</div>');
    CorpoEmail.Add('<table class="table-unstyled text-muted " style="font-family: Helvetica, Arial, sans-serif; mso-table-lspace: 0pt;');
    CorpoEmail.Add('mso-table-rspace: 0pt; border-spacing: 0px; border-collapse: collapse; width: 100%; max-width: 100%; color: #636c72;" border="0" cellspacing="0" cellpadding="0" bgcolor="transparent">');
    CorpoEmail.Add('<tbody><tr><td style="border-spacing: 0px; border-collapse: collapse; line-height: 24px; font-size: 16px; border-top-width: 0; border-bottom-width: 0;');
    CorpoEmail.Add('margin: 0;" align="left"></td>');
    CorpoEmail.Add('</tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table>');

    ACBrBoleto.EnviarEmail(Titulo.Sacado.Email ,'Farmasoft - Boleto dispon�vel para pagamento', CorpoEmail, True);

    CorpoEmail.Free;


   DM.CDSBuscaBoleto.Close;
   DM.CDSBuscaBoleto.ParamByName('ID_CLIENTE').Value := DM.CDSConsBoletosID_CLIENTE.Value;
   DM.CDSBuscaBoleto.ParamByName('DOCUMENTO').Value  := DM.CDSConsBoletosDOCUMENTO.Value;
   DM.CDSBuscaBoleto.Open;
   DM.CDSBuscaBoleto.Edit;
   DM.CDSBuscaBoletoENVIADOEM.Value := FormatDateTime('dd/mm/yyyy hh:mm', Now);
   DM.CDSBuscaBoletoENVIADO.Value   := 1;
   DM.CDSBuscaBoleto.Post;
   DM.CDSBuscaBoleto.ApplyUpdates(0);
  end;

 DM.CDSConsBoletos.Next;
 end;
  DM.CDSConsBoletos.EnableControls;

  lbProgress.Visible   := False;
  ProgressBar1.Visible := False;
  ShowMessage('E-mails enviados com sucesso!');
end;

function TfrmPrincipal.FormataNome(sNome: String): string;
const
  excecao: array[0..5] of string = (' da ', ' de ', ' do ', ' das ', ' dos ', ' e ');
var
  tamanho, j: integer;
  i: byte;
begin
  Result := AnsiLowerCase(sNome);
  tamanho := Length(Result);

  for j := 1 to tamanho do
    // Se � a primeira letra ou se o caracter anterior � um espa�o
    if (j = 1) or ((j>1) and (Result[j-1]=Chr(32))) then
      Result[j] := AnsiUpperCase(Result[j])[1];
  for i := 0 to Length(excecao)-1 do
    result:= StringReplace(result,excecao[i],excecao[i],[rfReplaceAll, rfIgnoreCase]);
end;

procedure TfrmPrincipal.FormCreate(Sender: TObject);
begin
  DM.CDSConsBoletos.Close;
  DM.CDSConsBoletos.Open;

  DT1.Date := Now;
  DT2.Date := Now;

  if not DirectoryExists(ExtractFilePath(Application.ExeName) + 'Boletos') then
  ForceDirectories(ExtractFilePath(Application.ExeName) + 'Boletos');
end;

procedure TfrmPrincipal.FormResize(Sender: TObject);
begin
  uFuncoes.DimensionarGrid(DBGrid1);
end;

procedure TfrmPrincipal.FormShow(Sender: TObject);
begin
  uFuncoes.DimensionarGrid(DBGrid1);

  if ParamStr(1) = 'FarmaSoftTecnologia' then
  begin
    DM.CDSConsBoletos.Filtered := False;
    DM.CDSConsBoletos.Filter   := 'ID_CLIENTE = ' + QuotedStr(ParamStr(2));
    DM.CDSConsBoletos.Filtered := True;
  end;
end;

procedure TfrmPrincipal.Image1Click(Sender: TObject);
begin
  try
    Application.CreateForm(TFrmSenha, FrmSenha);
    FrmSenha.ShowModal;
  finally
    FrmSenha.Free;
  end;


  if SenhaGerencial <> '' then
  begin
    DM.CDSSenha.Close;
    DM.CDSSenha.ParamByName('SENHA').Value := SenhaGerencial;
    DM.CDSSenha.Open;

    if DM.CDSSenha.RecordCount > 0 then
    begin
      if OpenDialog1.Execute then
      begin
        ImportarArquivo;
        DM.CDSConsBoletos.Close;
        DM.CDSConsBoletos.Open;
      end;
    end
    else
    begin
      ShowMessage('Senha Inv�lida');
    end;
  end; 
end;

procedure TfrmPrincipal.Image2Click(Sender: TObject);
begin
  CriarBoletoPDF;
end;

procedure TfrmPrincipal.Image3Click(Sender: TObject);
begin
  EnviarBoletoEmail;

  DM.CDSConsBoletos.Close;
  DM.CDSConsBoletos.Open;
end;

procedure TfrmPrincipal.ImportarArquivo;
var Titulo : TACBrTitulo;
    NomeArquivoRetorno, VLinha, logo  : String;
    vLiquidacao : Boolean;
    I, J ,VQtdeCarcA, VQtdeCarcB, VQtdeCarcC :Integer;
begin

  NomeArquivoRetorno        := OpenDialog1.FileName;
  AcbrBoleto.NomeArqRetorno := NomeArquivoRetorno;
  AcbrBoleto.LerRetorno;

  uFuncoes.ExibeMensagem('Aguarde, Importando Boletos...', True);
  for I := 0 to AcbrBoleto.ListadeBoletos.Count - 2 do
  begin

    //Localizo Duplicata no Softech
    DM.CDSConsDuplicatas.Close;
    DM.CDSConsDuplicatas.ParamByName('CHEQUE').Value := AcbrBoleto.ListadeBoletos.Objects[I].NumeroDocumento;
    DM.CDSConsDuplicatas.Open;

    if (DM.CDSConsDuplicatas.RecordCount > 0) and (DM.CDSConsDuplicatasCONTROLE.Value <> 'P') then
    begin
     //Verificando se J� existe esse boleto dentro do Boletos_emails
     DM.CDSBuscaBoleto.Close;
     DM.CDSBuscaBoleto.ParamByName('ID_CLIENTE').Value := DM.CDSConsDuplicatasID_CLIENTE.Value;
     DM.CDSBuscaBoleto.ParamByName('DOCUMENTO').Value  := DM.CDSConsDuplicatasCHEQUE.Value;
     DM.CDSBuscaBoleto.Open;

     if DM.CDSBuscaBoleto.RecordCount = 0 then
     begin
      //Inserindo Boletos Emails
      DM.CDSBuscaBoleto.Append;

      DM.CDSBuscaBoletoID_BOLETO_EMAIL.Value  := uFuncoes.RetornaId('ID_BOLETO_EMAIL');
      DM.CDSBuscaBoletoID_CLIENTE.Value       := DM.CDSConsDuplicatasID_CLIENTE.AsInteger;
      DM.CDSBuscaBoletoID_DUPLICATA.Value     := DM.CDSConsDuplicatasID_BOLETO.Value;
      DM.CDSBuscaBoletoDOCUMENTO.Value        := AcbrBoleto.ListadeBoletos.Objects[I].NumeroDocumento;
      DM.CDSBuscaBoletoCARTEIRA.Value         := AcbrBoleto.ListadeBoletos.Objects[I].Carteira;
      DM.CDSBuscaBoletoDT_DOCUMENTO.Value     := AcbrBoleto.ListadeBoletos.Objects[I].DataProcessamento;
      DM.CDSBuscaBoletoDT_VENCIMENTO.Value    := AcbrBoleto.ListadeBoletos.Objects[I].Vencimento;
      DM.CDSBuscaBoletoDT_PROCESSAMENTO.Value := AcbrBoleto.ListadeBoletos.Objects[I].DataProcessamento;
      DM.CDSBuscaBoletoNOSSO_NUMERO.Value     := AcbrBoleto.ListadeBoletos.Objects[I].NossoNumero;
      DM.CDSBuscaBoletoVL_DOCUMENTO.Value     := AcbrBoleto.ListadeBoletos.Objects[I].ValorDocumento;
      DM.CDSBuscaBoletoVL_ABATIMENTO.Value    := AcbrBoleto.ListadeBoletos.Objects[I].ValorAbatimento;
      DM.CDSBuscaBoletoVL_MORA_JUROS.Value    := AcbrBoleto.ListadeBoletos.Objects[I].ValorMoraJuros;
      DM.CDSBuscaBoletoENVIADO.Value          := 0;

      DM.CDSBuscaBoleto.Post;
      DM.CDSBuscaBoleto.ApplyUpdates(0);
     end
     else
     begin
      //Editando Boletos Emails
      DM.CDSBuscaBoleto.Edit;
      DM.CDSBuscaBoletoCARTEIRA.Value         := AcbrBoleto.ListadeBoletos.Objects[I].Carteira;
      DM.CDSBuscaBoletoDT_DOCUMENTO.Value     := AcbrBoleto.ListadeBoletos.Objects[I].DataProcessamento;
      DM.CDSBuscaBoletoDT_VENCIMENTO.Value    := AcbrBoleto.ListadeBoletos.Objects[I].Vencimento;
      DM.CDSBuscaBoletoDT_PROCESSAMENTO.Value := AcbrBoleto.ListadeBoletos.Objects[I].DataProcessamento;
      DM.CDSBuscaBoletoNOSSO_NUMERO.Value     := AcbrBoleto.ListadeBoletos.Objects[I].NossoNumero;
      DM.CDSBuscaBoletoVL_DOCUMENTO.Value     := AcbrBoleto.ListadeBoletos.Objects[I].ValorDocumento;
      DM.CDSBuscaBoletoVL_ABATIMENTO.Value    := AcbrBoleto.ListadeBoletos.Objects[I].ValorAbatimento;
      DM.CDSBuscaBoletoVL_MORA_JUROS.Value    := AcbrBoleto.ListadeBoletos.Objects[I].ValorMoraJuros;

      DM.CDSBuscaBoleto.Post;
      DM.CDSBuscaBoleto.ApplyUpdates(0);
     end;

    end;

  end;
  uFuncoes.ExibeMensagem('Aguarde, Importando Boletos...', False);
  ShowMessage('Arquivo ' + ExtractFileName(OpenDialog1.FileName) + ' Importado com Sucesso!');
end;



procedure TfrmPrincipal.Label4Click(Sender: TObject);
begin
   uFuncoes.ExibeMensagem('Aguarde, Selecionando Todos os Boletos...', True);

  DM.CDSConsBoletos.First;
  DM.CDSConsBoletos.DisableControls;

  while not DM.CDSConsBoletos.Eof do
  begin
    DM.CDSConsBoletos.Edit;
    if DM.CDSConsBoletosX.Value = 0 then
       DM.CDSConsBoletosX.Value := 1
    else
      DM.CDSConsBoletosX.Value  := 0;
    DM.CDSConsBoletos.Post;
    DM.CDSConsBoletos.Next;
  end;

  DM.CDSConsBoletos.EnableControls;
  uFuncoes.ExibeMensagem('Aguarde, Selecionando Todos os Boletos...', False);
end;

procedure TfrmPrincipal.Pesquisar;
var FiltroTexto, Filter, FiltroData1, FiltroData2 : String;
begin
   FiltroTexto := '';
   FiltroData1 := '';
   FiltroData2 := '';

   if SBPesquisar.Text <> '' then
   begin
      if uFuncoes.IsNumber(SBPesquisar.Text) then
     begin
        FiltroTexto := 'ID_CLIENTE = ' + QuotedStr(SBPesquisar.Text) + ' OR CNPJ =' + QuotedStr(SBPesquisar.Text);
     end
     else
     begin
       FiltroTexto := 'RAZAO_SOCIAL LIKE' + QuotedStr('%' + SBPesquisar.Text + '%') + ' OR CNPJ LIKE' + QuotedStr('%' + SBPesquisar.Text);
     end;

     SBPesquisar.Text := '';
   end;


   if DT1.Date <> Date then
   begin
     FiltroData1 := 'DT_VENCIMENTO >= ' + QuotedStr(DateToStr(DT1.Date));
   end;

   if DT2.Date <> Date then
   begin
     FiltroData2 := 'DT_VENCIMENTO <= ' + QuotedStr(DateToStr(DT2.Date));
   end;

   if FiltroTexto <> '' then
      Filter := Filter + FiltroTexto;

   if FiltroData1 <> '' then
   begin
      if Filter <> '' then
      Filter := Filter + ' AND ';

      Filter := Filter +  FiltroData1;
   end;

   if FiltroData2 <> '' then
   begin
      if Filter <> '' then
      Filter := Filter + ' AND ';

      Filter := Filter +  FiltroData2;
   end;


   DM.CDSConsBoletos.Close;
   DM.CDSConsBoletos.Open;

   DM.CDSConsBoletos.Filtered := False;
   DM.CDSConsBoletos.Filter   := Filter;
   DM.CDSConsBoletos.Filtered := True;
end;

procedure TfrmPrincipal.SBPesquisarInvokeSearch(Sender: TObject);
begin
 Pesquisar;
end;

end.
