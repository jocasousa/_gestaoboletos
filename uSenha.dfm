object FrmSenha: TFrmSenha
  Left = 0
  Top = 0
  BorderIcons = []
  Caption = 'Senha Gerencial'
  ClientHeight = 84
  ClientWidth = 234
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Century Gothic'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 17
  object Label1: TLabel
    Left = 40
    Top = 12
    Width = 154
    Height = 23
    Caption = 'Senha Gerencial'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Century Gothic'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object EdtSenha: TEdit
    Left = 27
    Top = 41
    Width = 173
    Height = 25
    CharCase = ecUpperCase
    PasswordChar = '#'
    TabOrder = 0
    OnKeyDown = EdtSenhaKeyDown
  end
end
