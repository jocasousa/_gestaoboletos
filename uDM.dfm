object DM: TDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 686
  Width = 1001
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 880
    Top = 32
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 880
    Top = 96
  end
  object FDConn: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'DriverID=FB')
    LoginPrompt = False
    BeforeConnect = FDConnBeforeConnect
    Left = 48
    Top = 32
  end
  object FDBuscaBoleto: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      
        'SELECT * FROM BOLETOS_EMAIL WHERE ID_CLIENTE = :ID_CLIENTE AND D' +
        'OCUMENTO = :DOCUMENTO')
    Left = 96
    Top = 136
    ParamData = <
      item
        Position = 1
        Name = 'ID_CLIENTE'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 2
        Name = 'DOCUMENTO'
        DataType = ftString
        ParamType = ptInput
        Size = 100
      end>
  end
  object DSPBuscaBoleto: TDataSetProvider
    DataSet = FDBuscaBoleto
    Left = 200
    Top = 136
  end
  object CDSBuscaBoleto: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_CLIENTE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'DOCUMENTO'
        ParamType = ptInput
        Size = 100
      end>
    ProviderName = 'DSPBuscaBoleto'
    Left = 304
    Top = 136
    object CDSBuscaBoletoID_BOLETO_EMAIL: TIntegerField
      FieldName = 'ID_BOLETO_EMAIL'
      Required = True
    end
    object CDSBuscaBoletoID_CLIENTE: TIntegerField
      FieldName = 'ID_CLIENTE'
    end
    object CDSBuscaBoletoID_DUPLICATA: TIntegerField
      FieldName = 'ID_DUPLICATA'
    end
    object CDSBuscaBoletoDOCUMENTO: TStringField
      FieldName = 'DOCUMENTO'
      Size = 100
    end
    object CDSBuscaBoletoDT_DOCUMENTO: TDateField
      FieldName = 'DT_DOCUMENTO'
    end
    object CDSBuscaBoletoDT_VENCIMENTO: TDateField
      FieldName = 'DT_VENCIMENTO'
    end
    object CDSBuscaBoletoDT_PROCESSAMENTO: TDateField
      FieldName = 'DT_PROCESSAMENTO'
    end
    object CDSBuscaBoletoNOSSO_NUMERO: TStringField
      FieldName = 'NOSSO_NUMERO'
      Size = 100
    end
    object CDSBuscaBoletoVL_DOCUMENTO: TFloatField
      FieldName = 'VL_DOCUMENTO'
    end
    object CDSBuscaBoletoVL_ABATIMENTO: TFloatField
      FieldName = 'VL_ABATIMENTO'
    end
    object CDSBuscaBoletoVL_MORA_JUROS: TFloatField
      FieldName = 'VL_MORA_JUROS'
    end
    object CDSBuscaBoletoCARTEIRA: TStringField
      FieldName = 'CARTEIRA'
      Size = 10
    end
    object CDSBuscaBoletoENVIADO: TIntegerField
      FieldName = 'ENVIADO'
    end
    object CDSBuscaBoletoENVIADOEM: TStringField
      FieldName = 'ENVIADOEM'
      Size = 30
    end
  end
  object FDParametros: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'SELECT'
      '    SERVIDORSMTPFINANCEIRO,'
      '    USUARIOSMTPFINANCEIRO,'
      '    SENHASMTPFINANCEIRO,'
      '    PORTASMTPFINANCEIRO'
      'FROM PARAMETROS')
    Left = 80
    Top = 504
  end
  object DSPParametros: TDataSetProvider
    DataSet = FDParametros
    Left = 200
    Top = 504
  end
  object CDSParametros: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DSPParametros'
    Left = 312
    Top = 504
    object CDSParametrosSERVIDORSMTPFINANCEIRO: TStringField
      FieldName = 'SERVIDORSMTPFINANCEIRO'
      Size = 50
    end
    object CDSParametrosUSUARIOSMTPFINANCEIRO: TStringField
      FieldName = 'USUARIOSMTPFINANCEIRO'
      Size = 50
    end
    object CDSParametrosSENHASMTPFINANCEIRO: TStringField
      FieldName = 'SENHASMTPFINANCEIRO'
      Size = 50
    end
    object CDSParametrosPORTASMTPFINANCEIRO: TStringField
      FieldName = 'PORTASMTPFINANCEIRO'
      Size = 50
    end
  end
  object FDConsCliente: TFDQuery
    Connection = FDConn
    Left = 96
    Top = 208
  end
  object DSPConsCliente: TDataSetProvider
    DataSet = FDConsCliente
    Left = 200
    Top = 208
  end
  object CDSConCliente: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DSPConsCliente'
    Left = 304
    Top = 208
  end
  object FDConsDuplicatas: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'SELECT * FROM DUPLICATAS WHERE CHEQUE = :CHEQUE')
    Left = 88
    Top = 288
    ParamData = <
      item
        Name = 'CHEQUE'
        DataType = ftString
        ParamType = ptInput
        Size = 10
        Value = Null
      end>
  end
  object DSPConsDuplicatas: TDataSetProvider
    DataSet = FDConsDuplicatas
    Left = 192
    Top = 288
  end
  object CDSConsDuplicatas: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftString
        Name = 'CHEQUE'
        ParamType = ptInput
        Size = 10
      end>
    ProviderName = 'DSPConsDuplicatas'
    Left = 296
    Top = 288
    object CDSConsDuplicatasID_BOLETO: TIntegerField
      FieldName = 'ID_BOLETO'
    end
    object CDSConsDuplicatasOLD: TStringField
      FieldName = 'OLD'
      Size = 10
    end
    object CDSConsDuplicatasSITUACAO: TStringField
      FieldName = 'SITUACAO'
      Size = 1
    end
    object CDSConsDuplicatasID_CLIENTE: TFloatField
      FieldName = 'ID_CLIENTE'
    end
    object CDSConsDuplicatasAREA: TStringField
      FieldName = 'AREA'
      Size = 1
    end
    object CDSConsDuplicatasCD_VENDED: TStringField
      FieldName = 'CD_VENDED'
      Size = 2
    end
    object CDSConsDuplicatasVALOR: TFloatField
      FieldName = 'VALOR'
    end
    object CDSConsDuplicatasDT_VENC: TDateField
      FieldName = 'DT_VENC'
    end
    object CDSConsDuplicatasDT_PAGAM: TDateField
      FieldName = 'DT_PAGAM'
    end
    object CDSConsDuplicatasCONTROLE: TStringField
      FieldName = 'CONTROLE'
      Size = 1
    end
    object CDSConsDuplicatasVL_PAGO: TFloatField
      FieldName = 'VL_PAGO'
    end
    object CDSConsDuplicatasFI_CLINICA: TStringField
      FieldName = 'FI_CLINICA'
      Size = 15
    end
    object CDSConsDuplicatasTP_PAGAM: TStringField
      FieldName = 'TP_PAGAM'
      Size = 1
    end
    object CDSConsDuplicatasCHEQUE: TStringField
      FieldName = 'CHEQUE'
      Size = 10
    end
    object CDSConsDuplicatasFORMA: TStringField
      FieldName = 'FORMA'
      Size = 2
    end
    object CDSConsDuplicatasDESCRICAOFORMA: TStringField
      FieldName = 'DESCRICAOFORMA'
      Size = 100
    end
    object CDSConsDuplicatasUSUARIO_BAIXA: TStringField
      FieldName = 'USUARIO_BAIXA'
      Size = 100
    end
    object CDSConsDuplicatasDESCRICAOSERVICO: TStringField
      FieldName = 'DESCRICAOSERVICO'
      Size = 100
    end
    object CDSConsDuplicatasTP_BOLETO: TStringField
      FieldName = 'TP_BOLETO'
      Size = 1
    end
    object CDSConsDuplicatasCHAVE: TStringField
      FieldName = 'CHAVE'
      Size = 10
    end
    object CDSConsDuplicatasTP_GERACAO: TStringField
      FieldName = 'TP_GERACAO'
      Size = 1
    end
    object CDSConsDuplicatasDT_GERACAO: TDateField
      FieldName = 'DT_GERACAO'
    end
  end
  object FDControle: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'SELECT * FROM CONTROLE WHERE CAMPO = :CAMPO')
    Left = 96
    Top = 56
    ParamData = <
      item
        Name = 'CAMPO'
        DataType = ftString
        ParamType = ptInput
        Size = 30
        Value = Null
      end>
  end
  object DSPControle: TDataSetProvider
    DataSet = FDControle
    Left = 200
    Top = 56
  end
  object CDSControle: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftString
        Name = 'CAMPO'
        ParamType = ptInput
        Size = 30
      end>
    ProviderName = 'DSPControle'
    Left = 304
    Top = 56
    object CDSControleCAMPO: TStringField
      FieldName = 'CAMPO'
      Required = True
      Size = 30
    end
    object CDSControleVALOR: TFloatField
      FieldName = 'VALOR'
    end
  end
  object FDConsBoletos: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'SELECT'
      'CLIENTES.ID_CLIENTE,'
      'CLIENTES.RAZAO_SOCIAL,'
      'CLIENTES.NOME_FANTASIA,'
      'CLIENTES.CNPJ,'
      'CLIENTES.ENDERECO,'
      'CLIENTES.NUMERO,'
      'CLIENTES.BAIRRO,'
      'CLIENTES.CIDADE,'
      'CLIENTES.CEP,'
      'CLIENTES.UF,'
      'CLIENTES.EMAIL,'
      'DUPLICATAS.CONTROLE AS STATUS,'
      'BOLETOS_EMAIL.*'
      'FROM BOLETOS_EMAIL'
      
        'INNER JOIN CLIENTES ON (BOLETOS_EMAIL.ID_CLIENTE = CLIENTES.ID_C' +
        'LIENTE)'
      
        'LEFT JOIN DUPLICATAS ON (BOLETOS_EMAIL.ID_DUPLICATA = DUPLICATAS' +
        '.ID_BOLETO)'
      'WHERE DUPLICATAS.CONTROLE <> '#39'P'#39)
    Left = 88
    Top = 360
    ParamData = <
      item
        Position = 1
        Name = 'ID_CLIENTE'
        DataType = ftString
        ParamType = ptInput
        Size = 30
      end
      item
        Position = 2
        Name = 'RAZAO_SOCIAL'
        DataType = ftString
        ParamType = ptInput
        Size = 50
      end
      item
        Position = 3
        Name = 'CNPJ'
        DataType = ftString
        ParamType = ptInput
        Size = 20
      end
      item
        Position = 4
        Name = 'DT1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Position = 5
        Name = 'DT2'
        DataType = ftDate
        ParamType = ptInput
      end>
  end
  object DSPConsBoletos: TDataSetProvider
    DataSet = FDConsBoletos
    Left = 192
    Top = 360
  end
  object CDSConsBoletos: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftString
        Name = 'ID_CLIENTE'
        ParamType = ptInput
        Size = 30
      end
      item
        DataType = ftString
        Name = 'RAZAO_SOCIAL'
        ParamType = ptInput
        Size = 50
      end
      item
        DataType = ftString
        Name = 'CNPJ'
        ParamType = ptInput
        Size = 20
      end
      item
        DataType = ftDate
        Name = 'DT1'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'DT2'
        ParamType = ptInput
      end>
    ProviderName = 'DSPConsBoletos'
    Left = 296
    Top = 360
    object CDSConsBoletosX: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'X'
    end
    object CDSConsBoletosID_CLIENTE: TFloatField
      FieldName = 'ID_CLIENTE'
    end
    object CDSConsBoletosRAZAO_SOCIAL: TStringField
      FieldName = 'RAZAO_SOCIAL'
      Size = 50
    end
    object CDSConsBoletosNOME_FANTASIA: TStringField
      FieldName = 'NOME_FANTASIA'
      Size = 30
    end
    object CDSConsBoletosCNPJ: TStringField
      FieldName = 'CNPJ'
    end
    object CDSConsBoletosSTATUS: TStringField
      FieldName = 'STATUS'
      Size = 1
    end
    object CDSConsBoletosID_BOLETO_EMAIL: TIntegerField
      FieldName = 'ID_BOLETO_EMAIL'
      Required = True
    end
    object CDSConsBoletosID_CLIENTE_1: TIntegerField
      FieldName = 'ID_CLIENTE_1'
    end
    object CDSConsBoletosID_DUPLICATA: TIntegerField
      FieldName = 'ID_DUPLICATA'
    end
    object CDSConsBoletosDOCUMENTO: TStringField
      FieldName = 'DOCUMENTO'
    end
    object CDSConsBoletosDT_DOCUMENTO: TDateField
      FieldName = 'DT_DOCUMENTO'
    end
    object CDSConsBoletosDT_VENCIMENTO: TDateField
      FieldName = 'DT_VENCIMENTO'
    end
    object CDSConsBoletosDT_PROCESSAMENTO: TDateField
      FieldName = 'DT_PROCESSAMENTO'
    end
    object CDSConsBoletosNOSSO_NUMERO: TStringField
      FieldName = 'NOSSO_NUMERO'
      Size = 100
    end
    object CDSConsBoletosVL_DOCUMENTO: TFloatField
      FieldName = 'VL_DOCUMENTO'
      currency = True
    end
    object CDSConsBoletosVL_ABATIMENTO: TFloatField
      FieldName = 'VL_ABATIMENTO'
      currency = True
    end
    object CDSConsBoletosVL_MORA_JUROS: TFloatField
      FieldName = 'VL_MORA_JUROS'
      currency = True
    end
    object CDSConsBoletosENDERECO: TStringField
      FieldName = 'ENDERECO'
      Size = 100
    end
    object CDSConsBoletosBAIRRO: TStringField
      FieldName = 'BAIRRO'
      Size = 50
    end
    object CDSConsBoletosCIDADE: TStringField
      FieldName = 'CIDADE'
      Size = 50
    end
    object CDSConsBoletosCEP: TStringField
      FieldName = 'CEP'
      Size = 9
    end
    object CDSConsBoletosUF: TStringField
      FieldName = 'UF'
      Size = 2
    end
    object CDSConsBoletosNUMERO: TStringField
      FieldName = 'NUMERO'
      Size = 100
    end
    object CDSConsBoletosCARTEIRA: TStringField
      FieldName = 'CARTEIRA'
      Size = 10
    end
    object CDSConsBoletosENVIADO: TIntegerField
      FieldName = 'ENVIADO'
    end
    object CDSConsBoletosEMAIL: TStringField
      FieldName = 'EMAIL'
      Size = 1000
    end
    object CDSConsBoletosENVIADOEM: TStringField
      DisplayWidth = 15
      FieldName = 'ENVIADOEM'
      Size = 30
    end
  end
  object FDSenha: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'SELECT * FROM USUARIOS WHERE CD_PERMISSAO = 1 AND SENHA = :SENHA')
    Left = 88
    Top = 424
    ParamData = <
      item
        Name = 'SENHA'
        DataType = ftString
        ParamType = ptInput
        Size = 20
        Value = Null
      end>
  end
  object DSPSenha: TDataSetProvider
    DataSet = FDSenha
    Left = 192
    Top = 424
  end
  object CDSSenha: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftString
        Name = 'SENHA'
        ParamType = ptInput
        Size = 20
      end>
    ProviderName = 'DSPSenha'
    Left = 296
    Top = 424
    object CDSSenhaCD_USUARIO: TFloatField
      FieldName = 'CD_USUARIO'
    end
    object CDSSenhaCD_PERMISSAO: TFloatField
      FieldName = 'CD_PERMISSAO'
    end
    object CDSSenhaNOME: TStringField
      FieldName = 'NOME'
      Size = 30
    end
    object CDSSenhaLOGIN: TStringField
      FieldName = 'LOGIN'
      Size = 30
    end
    object CDSSenhaSENHA: TStringField
      FieldName = 'SENHA'
    end
    object CDSSenhaID_TECNICO: TFloatField
      FieldName = 'ID_TECNICO'
    end
    object CDSSenhaPODEVENDER: TStringField
      FieldName = 'PODEVENDER'
      Size = 1
    end
    object CDSSenhaCAIXA: TStringField
      FieldName = 'CAIXA'
      Size = 1
    end
    object CDSSenhaVERCHAMADO: TStringField
      FieldName = 'VERCHAMADO'
      Size = 1
    end
    object CDSSenhaSTATUS: TStringField
      FieldName = 'STATUS'
      Size = 1
    end
  end
end
